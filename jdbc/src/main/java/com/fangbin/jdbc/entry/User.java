package com.fangbin.jdbc.entry;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.Resource;
import java.io.Serializable;

/**
 * @Author: FangBin
 * @Description:
 * @Date: Created in 17:38 2021/4/9
 * @ModifiedBy:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

    private Integer id;
    private String username;
    private Integer age;
}
