package com.fangbin.jdbc;

import com.fangbin.jdbc.entry.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

@SpringBootTest
class JdbcApplicationTests {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * @Description: 测试查询所有用户
     * @param: []
     * @return: void
     * @Author: FangBin
     * @Date: 2021/4/9
     */
    @Test
    public void listUser() {
        List<User> users = jdbcTemplate.query("select * from t_user", new BeanPropertyRowMapper<>(User.class));
        users.forEach(user -> System.out.println(user));
    }
}
